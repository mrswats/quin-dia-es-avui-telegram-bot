import logging
import os
import random
from datetime import datetime
from enum import Enum
from pathlib import Path

import dotenv
from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, Updater

dotenv.load_dotenv()

logging.basicConfig(
    level=logging.INFO,
    style="{",
    format="{name} -- [{levelname}] {asctime}: {message}",
)

random.seed()

TELEGRAM_API_TOKEN = os.getenv("TELEGRAM_API_TOKEN")
WEBHOOK_URL = os.getenv("WEBHOOK_URL")
PORT = int(os.getenv("PORT", "8443"))


class DiesDeLaSetmana(Enum):
    diumenge = "0"
    dilluns = "1"
    dimarts = "2"
    dimecres = "3"
    dijous = "4"
    divendres = "5"
    dissabte = "6"


def quin_dia_es_avui() -> DiesDeLaSetmana:
    avui_num = datetime.today().strftime("%w")

    for dia in DiesDeLaSetmana:
        if dia.value == avui_num:
            return dia


def imatge_i_missatge(avui: DiesDeLaSetmana) -> (str, str):

    if random.randint(0, 69) == 69:
        return "dia_wimdy.jpg", "Fa vemtet"

    match avui:
        case DiesDeLaSetmana.dilluns:
            filename = random.choices(["dilluns_joder.jpg", "dilluns_al_reves.png"], weights=[9, 1]).pop()
            message = "Per què?!"
        case DiesDeLaSetmana.dimarts:
            filename = "dimarts_garfield.jpg"
            message = "Tinc son"
        case DiesDeLaSetmana.dimecres:
            filename = random.choice(
                [
                    "dimecres_haddock.jpg",
                    "dimecres_alt_haddock.jpg",
                    "dimecres_haddock_llengua.jpg",
                    "dimecres_nomes_es_dimecres.jpg",
                    "dimecres_plats_bruts.jpeg",
                ]
            )
            message = "Ufff"
        case DiesDeLaSetmana.dijous:
            filename = "dijous_concepte.jpg"
            message = "QUIN CONCEPTE!"
        case DiesDeLaSetmana.divendres:
            filename = random.choices(["divendres_gracies.jpg", "divendres_garfield.jpg"], weights=[1, 9]).pop()
            message = "Gràcies a déu que és divendres!"
        case DiesDeLaSetmana.dissabte:
            filename = "dissabte_dormint.jpg"
            message = ""
        case DiesDeLaSetmana.diumenge:
            filename = "diumenge_cansat.png"
            message = "Nooo"

    return filename, message


def dia(update: Update, context: CallbackContext) -> None:

    avui = quin_dia_es_avui()
    filename, message = imatge_i_missatge(avui)

    with open(Path("./pics") / filename, "rb") as imatge:
        update.message.reply_photo(photo=imatge, caption=message)


def start_bot():
    updater = Updater(TELEGRAM_API_TOKEN)
    updater.dispatcher.add_handler(CommandHandler("dia", dia))

    if os.getenv("ENV"):
        updater.start_webhook(
            listen="0.0.0.0",
            port=PORT,
            url_path=TELEGRAM_API_TOKEN,
            webhook_url=f"{WEBHOOK_URL}/{TELEGRAM_API_TOKEN}",
        )
    else:
        logging.info("Starting in local polling mdoe")
        updater.start_polling()

    updater.idle()


if __name__ == "__main__":
    start_bot()
